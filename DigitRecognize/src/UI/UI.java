package UI;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.plaf.FontUIResource;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.awt.event.*;

import backend.*;
public class UI {

   //set JFrame width/height to integer
    private static final int FRAME_WIDTH = 1200;
    private static final int FRAME_HEIGHT = 628;

   //Initialize variables
    private DrawDigit_Area drawArea;
    
    private JFrame mainFrame;
    
    private JPanel mainPanel;
    private JPanel drawDigit_predictionPanel;  
    private JPanel recognizePanel;
    private JPanel confidencePanel;
    private JPanel outputPanel;
    
    private JFileChooser fileChooser;
    
    private final Font sansSerifBold = new Font("SansSerif", Font.BOLD, 18);

    private JButton openFileBtn;
	private JTextField fileNameTxt;
	private JLabel fileNameLbl;
	private JLabel image;
	private JLabel predictNumber;
	private JLabel accuracyLabel;
	private String selectFile = "";
	private JProgressBar progressBar;

	//ImageModel imgModel;
	
	//set up exceptions
    public UI() throws Exception {
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        UIManager.put("Button.font", new FontUIResource(new Font("Dialog", Font.BOLD, 18)));
    }

    //Initialize GUI class
    public void initUI() {
        //create main frame
        mainFrame = createMainFrame();
        //create main panel
        mainPanel = new JPanel();
        //set panel to border layout
        mainPanel.setLayout(new BorderLayout());
        //call addTopPanel function
        addTopPanel();
        //create drawAndDigitPredictionPanel panel and give it a grid layout
        drawDigit_predictionPanel = new JPanel(new GridLayout());
        //call addActionPanel function
        addActionPanel();
        //call addDrawAreaAndPredictionArea function
        addDrawAreaAndPredictionPanel();
        //add addDrawAreaAndPredictionArea to the center of the main panel
        mainPanel.add(drawDigit_predictionPanel, BorderLayout.CENTER);
        //add mainpanel to the center of the mainFrame
        mainFrame.add(mainPanel, BorderLayout.CENTER);
        //make sure the mainframe is visible when Initialized
        mainFrame.setVisible(true);

    }

    private void addActionPanel() {
    	//create button to recognize digit
        JButton recognize = new JButton("Recognize Digit With KNN");
        //add actionlistener to recognize digit button
        recognize.addActionListener(e -> {
			JOptionPane.showMessageDialog(new JFrame(), "Loading... will take a few minutes to process.");
        	   //create new image from the users drawn digit
                Image drawImage = drawArea.getImage();
               //create a buffered image from drawImage
                BufferedImage sbi = toBufferedImage(drawImage);
               //pass through the image controller
		        ImageController Image_c = new ImageController();
		        Image_c.setImage(sbi);
		        //set image to greyscale
		        Image_c.convertRGBToGrayscale(Image_c.getImage());
		        //resize image to match dataset
		        Image_c.resizeGreyScaleImage(28, 28);
		        MNIST_reader knn = new MNIST_reader();
		        knn.setImageController(Image_c);
		        knn.loadMNIST();
                //get confidence and predicted digit
		        double confidenceMeasure = knn.confidence(8); 
		        int recognizedDigit = knn.getDigit();
		        //create label to display predicted number/accuracy
		        predictNumber = new JLabel("");
		        predictNumber.setForeground(Color.RED);
                predictNumber.setFont(new Font("SansSerif", Font.BOLD, 68));
		        accuracyLabel = new JLabel(" ");
		        accuracyLabel.setForeground(Color.GREEN);
                accuracyLabel.setFont(new Font("SansSerif", Font.BOLD, 68));
		        predictNumber.setText("" + recognizedDigit);
		        accuracyLabel.setText(confidenceMeasure + "%");
		        
		        image.setIcon( null );    
		        
		        confidencePanel.removeAll();
		        outputPanel.removeAll();
                outputPanel.add(predictNumber);
                confidencePanel.add(accuracyLabel);
                confidencePanel.updateUI();
                outputPanel.updateUI();
            });
        

        //create clear button
        JButton clear = new JButton("Clear");
        //actionlistener to repaint drawarea white 
        clear.addActionListener(e -> {
                drawArea.setImage(null);
                drawArea.repaint();
                drawDigit_predictionPanel.updateUI();
                image.setIcon( null );    
                confidencePanel.removeAll();
		        outputPanel.removeAll();
            });
        
        //create panels to make appropriate layout
        JPanel layer1Panel = new JPanel();
        layer1Panel.setLayout(new BoxLayout(layer1Panel, BoxLayout.Y_AXIS));
        layer1Panel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
                "Action Buttons",
                TitledBorder.LEFT,
                TitledBorder.TOP, sansSerifBold, Color.BLACK));
        
        JPanel actionPanel = new JPanel(new GridLayout(4, 1));
        
        JPanel ImagePanel = new JPanel();
        ImagePanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
                "Chosen Image",
                TitledBorder.LEFT,
                TitledBorder.TOP, sansSerifBold, Color.BLACK));
        image = new JLabel(" ");
        ImagePanel.add(image);

        actionPanel.add(recognize);
        actionPanel.add(clear);
        actionPanel.add(ImagePanel);
        
        layer1Panel.add(actionPanel);
        layer1Panel.add(ImagePanel);

        mainPanel.add(layer1Panel, BorderLayout.EAST);
    }

    private void addDrawAreaAndPredictionPanel() {

        drawArea = new DrawDigit_Area();
        
        drawDigit_predictionPanel.add(drawArea);

        recognizePanel = new JPanel();
    	confidencePanel = new JPanel();
    	outputPanel = new JPanel();
    	
    	confidencePanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
                "Confidence Level",
                TitledBorder.LEFT,
                TitledBorder.TOP, sansSerifBold, Color.BLACK));
    	outputPanel.  setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
                "Predicted Number",
                TitledBorder.LEFT,
                TitledBorder.TOP, sansSerifBold, Color.BLACK));
    	
    	
    	recognizePanel.setLayout(new BoxLayout(recognizePanel, BoxLayout.Y_AXIS));
    	recognizePanel.add(confidencePanel);
    	recognizePanel.add(outputPanel);
        
    	drawDigit_predictionPanel.add(recognizePanel);
    }
    
    
    private void addTopPanel() {
  
        JPanel fileSelectPanel = new JPanel();
        
        fileNameLbl = new JLabel ("File name: ");
        fileNameTxt = new JTextField(50);
        fileNameTxt.setEnabled(false);        
        openFileBtn = new JButton("Open File");
        
        progressBar = new JProgressBar(0, 100);        
        progressBar.setValue(0);
        progressBar.setStringPainted(true);
		
        // Add components to the JPanel
        fileSelectPanel.add(fileNameLbl);
        fileSelectPanel.add(fileNameTxt);
        fileSelectPanel.add(openFileBtn);
        fileSelectPanel.add(progressBar);
        
		openFileBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				File selectedFile = showFileChooserDialog();
				
				if(selectedFile != null) {
					fileNameTxt.setText(selectedFile.getAbsolutePath());
					selectFile = selectedFile.getAbsolutePath();
					try {
	    				if (selectFile != null)  {
	    					//display dialog to warn user of how long it will take
	    					JOptionPane.showMessageDialog(new JFrame(), "Loading... will take a few minutes to process.");
                            //pass selected image through file handler
	    					ImageFileHandler fileHandler = new ImageFileHandler(selectFile); 
	    					//file handler reads file
	    			        fileHandler.readFile(); 
	    			        
	    			        ImageController Image_c = new ImageController();
	    			        //set image to image controller
	    			        Image_c.setImage(fileHandler.getImage());
	    			        //convert to greyscale
	    			        Image_c.convertRGBToGrayscale(Image_c .getImage());
	    			        //resize image to match dataset
	    			        Image_c.resizeGreyScaleImage(28, 28);
	    			       
	    			        MNIST_reader knn = new MNIST_reader();
	    			       //pass image though MNIST_reader
	    			        knn.setImageController(Image_c);
	    			        //compare images
	    			        knn.loadMNIST();
                            //get confidence and predicted digit
	    			        double confidenceMeasure = knn.confidence(4); 
	    			        int recognizedDigit = knn.getDigit();

	    			        //repainted draw area to white as it is not needed
	    	                drawArea.setImage(null);
	    	                drawArea.repaint();
	    	                drawDigit_predictionPanel.updateUI();
	    			        
	    			        //create label to display predicted number
	    			        predictNumber = new JLabel("");
	    			        predictNumber.setForeground(Color.RED);
	    	                predictNumber.setFont(new Font("SansSerif", Font.BOLD, 68));
	    			        accuracyLabel = new JLabel(" ");
	    			        accuracyLabel.setForeground(Color.GREEN);
	    	                accuracyLabel.setFont(new Font("SansSerif", Font.BOLD, 68));
	    	                predictNumber.setText("" + recognizedDigit);
	    			        accuracyLabel.setText(confidenceMeasure + "%");
	    			       
	    			        confidencePanel.removeAll();
	    			        outputPanel.removeAll();
	    	                outputPanel.add(predictNumber);
	    	                confidencePanel.add(accuracyLabel);
	    	                confidencePanel.updateUI();
	    	                outputPanel.updateUI();

	    				}
	    			} catch (Exception ex) {
	    				ex.printStackTrace();
	    			}

				}
				else
					fileNameTxt.setText("No file selected");	
			}
		});	
        mainPanel.add(fileSelectPanel, BorderLayout.NORTH);
    }
    
    //file chooser function
	private File showFileChooserDialog() {
		//set up file chooser
		fileChooser = new JFileChooser();
		fileChooser.setCurrentDirectory(new 
				File(System.getProperty("user.home")));
	    int status = fileChooser.showOpenDialog(this.mainFrame);
	    File selected_file = null;
	    if (status == JFileChooser.APPROVE_OPTION) {
	        selected_file = fileChooser.getSelectedFile();
	        try {
	        	//get image of selected file and add it to JLabel(image) to display the chosen image on the JPanel, resize image to 200 by 200
                image.setIcon(new ImageIcon(ImageIO.read(selected_file).getScaledInstance(150, 150, Image.SCALE_DEFAULT)));
            } catch (IOException e) {
                e.printStackTrace();
            }     
	    }
	    return selected_file;
	}

    public static BufferedImage toBufferedImage(Image img) {
        // Create a buffered image
        BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);
        // Draw the image on to the buffered image
        Graphics2D bGr = bimage.createGraphics();
        bGr.drawImage(img, 0, 0, null);
        bGr.dispose();
        // Return the buffered image
        return bimage;
    }

    //method to create and display the main Frame
    private JFrame createMainFrame() {
        JFrame mainFrame = new JFrame();
        mainFrame.setTitle("Digit Recognizer");
        mainFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        //give mainframe height and width 
        mainFrame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
        mainFrame.setLocationRelativeTo(null);
        
        mainFrame.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosed(WindowEvent e) {
                    System.exit(0);
                }
            });

        return mainFrame;
    }


}