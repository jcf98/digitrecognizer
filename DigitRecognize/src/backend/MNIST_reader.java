package backend;
import java.awt.image.*;
import java.util.*;
import java.io.*; 


public class MNIST_reader {
	
	private ImageController Image_controller = null;
	// import MNIST images
	String train_label_filename = "/Users/jensen/dataSet/train-labels-idx1-ubyte";
	String train_image_filename = "/Users/jensen/dataSet/train-images-idx3-ubyte";
	
	FileInputStream in_stream_labels = null;
	FileInputStream in_stream_images = null;
	
	DataInputStream dataStreamLables = null; 
	DataInputStream dataStreamImages = null; 
	
	private MNIST_setters[] currentDataItems = null;
	private int recognizedDigit;
	
	public ImageController getImageController() {
		return this.Image_controller;
	}
	
	public void setImageController (ImageController providedIC) {
		this.Image_controller = providedIC;
	}
	
	public MNIST_setters[] getDIArray() {
		return this.currentDataItems;
	}
	
	public void setDIArray(MNIST_setters[] providedDIArray) {
		this.currentDataItems = providedDIArray;
	}	
	
	public void loadMNIST() { 
		
		try {
			in_stream_labels = new FileInputStream(new File(train_label_filename));
			in_stream_images = new FileInputStream(new File(train_image_filename));
	
			dataStreamLables = new DataInputStream(in_stream_labels);
			dataStreamImages = new DataInputStream(in_stream_images); 
			
			int labels_start_code = dataStreamLables.readInt(); 
			int images_start_code = dataStreamImages.readInt();
			
			int labelCount = dataStreamLables.readInt();
			int imageCount = dataStreamImages.readInt();
			
			currentDataItems = new MNIST_setters[labelCount]; 
			
			int imageHeight = dataStreamImages.readInt(); 
			int imageWidth = dataStreamImages.readInt();
			
			
			
			int imageSize = imageHeight * imageWidth; 
			byte[] labelData = new byte[labelCount];
			byte[] imageData = new byte[imageSize * imageCount];
			BufferedImage tempImage;
			
			dataStreamLables.read(labelData);
			dataStreamImages.read(imageData);
			
			for (int currentRecord = 0; currentRecord < labelCount; currentRecord++) {
				int currentLabel = labelData[currentRecord];				
				MNIST_setters newlabel_image = new MNIST_setters();
				newlabel_image.setMNIST_label(currentLabel);			
				tempImage = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_ARGB);				
				int[][] imageDataArray = new int[imageWidth][imageHeight];
				for (int row = 0; row < imageHeight; row++) {
					for(int column = 0; column < imageWidth; column++) { 
						imageDataArray[column][row] = imageData[(currentRecord * imageSize)+((row*imageWidth) + column)] | 0xFF000000;
						tempImage.setRGB(column, row, imageDataArray[column][row]);
					}
					
				}
				
				newlabel_image.setMNIST_bufferedImage(tempImage); 
				currentDataItems[currentRecord] = newlabel_image;
			}
			if (in_stream_labels != null) {
				in_stream_labels.close();
			}
			if (in_stream_images != null) {
				in_stream_images.close();
			}			
		} catch (FileNotFoundException fn) {
			fn.printStackTrace();	   
		} catch (IOException e)	{
		   
		   e.printStackTrace();
	   }
	
	}
	
	public void sort_algorithm() {  
	    int n = currentDataItems.length;  
	    MNIST_setters tempDI = null;  
	    for(int i = 0; i < n; i++){  
	    	for(int j = 1; j < (n-i); j++){  
	    		if(currentDataItems[j - 1].getKNN_distance() > currentDataItems[j].getKNN_distance()){                       
                   tempDI = currentDataItems[j - 1];  
                   currentDataItems[j ] = currentDataItems[j-1];  
                   currentDataItems[j] = tempDI;  
	    		}                  
	    	}  
	    }	
	}
	
	public double confidence (int k) {   
		int maxCLabel = 0; 
	    int maxCount = 0;
	    try {
	    	this.sort_algorithm(); 
			int[] l_Array = new int [k];
			MNIST_setters[] tempMDIArray = this.getDIArray();
			for(int internalCount = 0; internalCount < k; internalCount++) { 
				MNIST_setters label_Index = tempMDIArray[internalCount];
				l_Array[internalCount] = label_Index.getMNIST_label();
			}
			int [][ ] countTrackingArray = new int[9][1];
			for (int selectedNumber = 1; selectedNumber <= 9; selectedNumber++) {
				int totalCount = 0; 
				for(int actualCount = 0; actualCount < l_Array.length; actualCount++) {
					if (l_Array[actualCount] == selectedNumber) {
						totalCount = totalCount + 1;
						
					}
				}
				countTrackingArray[(selectedNumber-1) ][0] = totalCount;			
			}
			for (int iteratedNumber =  0; iteratedNumber < 9; iteratedNumber++) {
				if (maxCount < countTrackingArray[iteratedNumber][0]) {
					maxCount = countTrackingArray[iteratedNumber][0]; 
					maxCLabel  = iteratedNumber + 1; 
				}
			}	

	    	 } catch (Exception e) {
	    		 e.printStackTrace();
	    	 }
	    	this.setRecogniseDigit(maxCLabel);
		    double accuracyratio = ((double) maxCount / k) * 100; 
			return accuracyratio;	
		}
	

	public int getDigit() {
		return this.recognizedDigit;
	}
	
	public void setRecogniseDigit( int suppliedDidgit) {
		this.recognizedDigit = suppliedDidgit;
	}
	

}
