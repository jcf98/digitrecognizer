package backend;
import java.awt.image.*;
import java.io.*;

public class MNIST_setters {
	
	private int MNIST_label; 
	private double KNN_distance; 
	private BufferedImage MNIST_bufferedImage;
	
	public MNIST_setters() {	}
	 
	public int getMNIST_label() {
		return this.MNIST_label;
	}
	
	public void setMNIST_label(int Given_label) {
		this.MNIST_label = Given_label;
	}
	
	public BufferedImage getMNIST_bufferedImage()  {
		return this.MNIST_bufferedImage;
	}

	public void setMNIST_bufferedImage(BufferedImage tempimage) {
		this.MNIST_bufferedImage = tempimage;
	}
	
	public double getKNN_distance() {
		return KNN_distance;
	} 
	
	public void setKnnDistanceValue(double tempValue) {
		this.KNN_distance = tempValue;
	}
}